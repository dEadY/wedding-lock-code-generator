package com.hochzeit.main;

import java.util.Scanner;
import com.hochzeit.helper.Generator;

public class LockCodeGenerator {

    public static void main (String args[]) {

        System.out.println(
                "Hier das Systempasswort eingeben, um die Kombination" +
                " für das schwarze Schloss zu berechnen" +
                "\n(mehrfache Eingabe gefolgt von ENTER wird auch akzeptiert):");

        while(true) {

            //region Hier bereiten wir alles vor, damit wir von der Konsole einlesen können
            //Wir lesen eine eingabe von der Konsole ein.
            Scanner in = new Scanner(System.in);
            //Jede Zeile die mit ENTER ended, wird sogleich verarbeitet
            String consoleInput = in.nextLine();
            //endregion

            //Das komplizierte Zeug lassen wir die Klasse Generator erledigen
            Generator lockCodeGenerator = new Generator(consoleInput);
            String lockCode = lockCodeGenerator.generateSomeCode();

            //Wir zeigen die Lösung für die Eingabe an
            System.out.println("Der Code für das Schloss ist: " + lockCode);
        }
    }
}
