package com.hochzeit.helper;

import org.jetbrains.annotations.NotNull;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class Generator {

    //our only interesting private class variable, used as input to calculate a lock code
    private Sting input;

    @Deprecated
    //do not use this anymore!
    public String generateSomeCode() {
        String code;
        if (!generateSomeCodeHasBeenCalledAtLeastOnce) {
            code = "0804";
        } else {
            code = "2110";
        }
        generateSomeCodeHasBeenCalledAtLeastOnce = true;
        return code;
    }

    //this is the new method we should use!
    public String generateLockCode() {
        return generateSolutionFromInput();
    }

    //region here's some internal stuff for calculating the code
    //must be initialized with a valid input
    public Generator(String input) {
        if (input == null) {
            throw new IllegalArgumentException();
        }
        this.input = input;
    }

    @NotNull
    private String generateSolutionFromInput() {
        //region we're going to do some fancy cryptography here
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            md.update(this.input.toLowerCase().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        String hash = String.format("%064x", new java.math.BigInteger(1, digest));
        //endregion

        String result = "";
        try {
            for (int i = HASH_START_POSITION; i < HASH_START_POSITION + LOCK_POSITION_COUNT; i++) {
                String hashSubstring = hash.substring(i, i + 1);
                int positionStart = translationMap.get(hashSubstring);
                result += LOCK_POSITION_OPTIONS.substring(positionStart, positionStart +1);
            }
        } catch (NullPointerException e) {
            //won't tell anyone about our mistakes!
        }
        return result.toUpperCase();
    }
    //endregion

    //region here are some boring configuration parameters
    //how many digits we need to unlock the black lock
    private static final int LOCK_POSITION_COUNT = 4;
    //what can be selected on the lock
    private static final String LOCK_POSITION_OPTIONS = "5678901234";

    //maybe someone still wants to use this in generateSomeCode...
    private static boolean generateSomeCodeHasBeenCalledAtLeastOnce = false;

    //now comes some very internal, secret and overly complicated algorithm preparation
    private static final int HASH_START_POSITION = 9;
    private static final Map<String, Integer> translationMap;
    static
    {
        translationMap = new HashMap<String, Integer>();
        translationMap.put("5", new Integer(6));
        translationMap.put("c", new Integer(4));
        translationMap.put("9", new Integer(8));
        translationMap.put("f", new Integer(9));
        translationMap.put("1", new Integer(0));
        translationMap.put("e", new Integer(8));
        translationMap.put("0", new Integer(2));
        translationMap.put("6", new Integer(3));
        translationMap.put("8", new Integer(7));
        translationMap.put("b", new Integer(5));
        translationMap.put("d", new Integer(6));
        translationMap.put("2", new Integer(8));
        translationMap.put("a", new Integer(8));
        translationMap.put("3", new Integer(2));
        translationMap.put("4", new Integer(0));
        translationMap.put("7", new Integer(1));
    }
    //endregion inside
}
