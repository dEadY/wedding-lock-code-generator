package com.hochzeit.helper;

import static org.junit.jupiter.api.Assertions.*;

class GeneratorTest {
    @org.junit.jupiter.api.Test
    void generateLockCodeForWrongInput() {
        com.hochzeit.helper.Generator generator = new com.hochzeit.helper.Generator("asdf");
        assertEquals("9123", generator.generateLockCode());
    }

    @org.junit.jupiter.api.Test
    void generateLockCodeForCorrectInput() {
        com.hochzeit.helper.Generator generator = new com.hochzeit.helper.Generator("TheBird");
        assertEquals("1337", generator.generateLockCode());
    }

}